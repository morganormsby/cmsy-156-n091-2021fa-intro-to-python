import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.CRITICAL, format=loggingformat)

# Problem 1: MagicDates
# The date June 10, 1960, is special because when it is written numerically, the month time the day equals
# the year:
# 6/10/60 --> 6*10 = 60
# Design a program that asks the user to enter a month (in numeric form), a day, and a two-digit year in three
# separate inputs. The program should determine whether the month times the day equals the year. If so, it should
# print,"This date is magic!" Otherwise, it should print, "This date is not magic."


def default_function(input_day=None,input_month=None,input_year=None):
    logging.debug("Starting execution of function")
    logging.debug(input_day)
    logging.debug(input_month)
    logging.debug(input_year)

    # lets see if we were passed any arguments in the function directly. If not, we'll prompt the user for the values.
    if input_day == None or input_month == None or input_year == None:
        logging.debug("missing function arguments, prompting user.")
        input_day = input('Enter the Day:\n')
        input_month = input("Enter the Month:\n")
        input_year = input("Enter the Year:\n")

    # lets see if we can do the math on the inputs we were given:
    try:
        # lets do the math to see if the date is magic:
        if int(input_day) * int(input_month) == int(input_year):
            logging.debug("magic.")
            print("This date is magic!")

        # looks like the date isn't magic:
        else:
            logging.debug("Not Magic")
            print("This date is not magic!")

    # maybe we didn't get an integer in the inputs?
    except (TypeError, ValueError):
        logging.debug("did we get a integer in the input?")
if __name__ == '__main__':
    default_function()