# Workbench 1:
if x > 100:
    y = 20
    x = 40

# Workbench 2:
if a < 10:
    b = 0
    c = 1

# Workbench 3:
if a < 10:
    b = 0
else:
    b = 99

# Workbench 4
if score >= A_score:
    print('Your grade is A.')
else:
    if score >= B_score:
        print('Your grade is B.')
    else:
        if score >= C_score:
            print('Your grade is C.')
        else:
            if score >= D_score:
                print('your grade is D.')
            else:
                print('Your grade is F.')

# Workbench 5
if amount1 > 10:
    if amount2 < 100:
        if amount1 > amount2:
            print(amount1)
        else:
            print(amount2)

# Workbench 6
if speed < 56 and speed > 24:
    print('Speed is normal.')
else:
    print('speed is abnormal')

# Workbench 7
if points > 9 and points < 51:
    print('Valid Points')
else:
    print('invalid points')


