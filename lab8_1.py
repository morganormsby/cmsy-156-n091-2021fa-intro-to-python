import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.CRITICAL, format=loggingformat)

def default_function():
    logging.debug("Starting execution of function")

    # predefine some variables

    number_dictionary = {}
    output_number = ''

    # lets get a number from the user:
    input_string = input('Please enter a phone number to convert:\n')

    #lets put the phone number into a dictionary to work with it and keep it in order.

    for i, v in enumerate(input_string):
        logging.debug(i)
        logging.debug(v)

        number_dictionary[i] = v

    logging.debug(number_dictionary)

    # we'll loop over the individual numbers and convert letters as needed.
    for index, translate_number in number_dictionary.items():
        logging.debug(translate_number)
        if str(translate_number.lower()) in ('a','b','c'):
            logging.debug("ABC")
            number_dictionary[index] = 2
        elif str(translate_number.lower()) in ('d','e','f'):
            logging.debug("def")
            number_dictionary[index] = 3
        elif str(translate_number.lower()) in ('g','h','i'):
            logging.debug("ghi")
            number_dictionary[index] = 4
        elif str(translate_number.lower()) in ('j', 'k', 'l'):
            logging.debug("jkl")
            number_dictionary[index] = 5
        elif str(translate_number.lower()) in ('m','n','o'):
            logging.debug("mno")
            number_dictionary[index] = 6
        elif str(translate_number.lower()) in ('p','q','r','s'):
            logging.debug("pqrs")
            number_dictionary[index] = 7
        elif str(translate_number.lower()) in ('t','u','v'):
            logging.debug("tuv")
            number_dictionary[index] = 8
        elif str(translate_number.lower()) in ('w','x','y','z'):
            logging.debug("wxyz")
            number_dictionary[index] = 9


    logging.debug(number_dictionary)

    # we'll loop over the dictionary and combine it's entries back into a readable number:

    for number_item in number_dictionary.values():
        # append strings to the variable
        output_number += str(number_item)

    # lets print the result

    print(output_number)

    #lets ask the user if the want to convert another number.

    if input('Would you like to convert another? y/n:\n') == 'y':
        default_function()

if __name__ == '__main__':
    default_function()



