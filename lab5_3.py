import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.CRITICAL, format=loggingformat)


# Suppose you have a certain amount of money in a savings account that earns compound monthly interest, and you want
# to calculate the amount that you will have after a specific number of months. The formula is as follows:
# f = p * (1 + i)^t
# • f is the future value of the account after the specified time period.
# • p is the present value of the account.
# • i is the monthly interest rate.
# • t is the number of months.
# Write a program that takes the account's present value, monthly interest rate, and the number of months that the
# money will be left in the account as three inputs from the user. The program should pass these values to a function
# that returns the future value of the account, after the specified number of months. The program should print the
# account's future value.

def default_function():

    try:
        # Lets get the current balance, the interest rate and the # of months from the user:
        prompted_account_present_value = float(input("Please enter the current account balance:\n$"))
        prompted_interest_rate = float(input("Please enter the interest rate:\n%")) / 100
        prompted_number_of_months = int(input("Please enter the number of months:\n"))
    # Looks like we didnt get an expected input.
    except(ValueError):
        print("That wasn't the kinda value we were looking for.")

    # lets call the function to calculate the balance in the future:
    account_future_balance = calculation_function(prompted_account_present_value,prompted_interest_rate,prompted_number_of_months)

    # lets print that math out:
    print("Account balance in " + str(prompted_number_of_months) + " months will be $" + str(account_future_balance))

def calculation_function(account_present_value = None,monthly_interest_rate = None,number_of_months = None):
    logging.debug("Starting execution of function")
    logging.debug(account_present_value)
    logging.debug(monthly_interest_rate)
    logging.debug(number_of_months)

    #lets take the vaules we were passed and do the math required:
    function_result = round(account_present_value * (1 + monthly_interest_rate) ** number_of_months,2)

    #lets return that result:
    return(function_result)

if __name__ == '__main__':
    default_function()