
import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.DEBUG, format=loggingformat)

def default_function():
    logging.debug("Starting execution of function")
    import re

    # We'll need to check if email address are email addresses. let make a function to do that.

    def check_email_address(email):
        regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
        if (re.search(regex, email)):
            return True
        else:
            return False

    def application_menu():
        print("Please make a selection:\n"
              "1: Look up an e-mail address\n"
              "2: Add a new name and e-mail address\n"
              "3: Change an e-mail address\n"
              "4: Delete a name and e-mail address\n"
              "5: Save address book and exit.\n")
        user_entry = input("Enter Selection: ")
        if user_entry == 1:
            logging.debug(user_entry)
            # do stuff
        elif user_entry == 2:
            logging.debug(user_entry)
            address_to_add = input('Please enter an e-mail address\n')
            name_to_add = input('Please enter a Name\n')
            phonebook_dictionary[address_to_add] = name_to_add
            # do other stuff
        elif user_entry == 3:
            logging.debug(user_entry)
            # do other stuff
        elif user_entry == 4:
            logging.debug(user_entry)
            # do other stuff
        elif user_entry == 5:
            logging.debug(user_entry)
            # do other stuff
        else:
            logging.debug(user_entry)
            # invalid selection made.




    # initalizing the phonebook dictionary.

    phonebook_dictionary = {}

    # reading in the existing entries in the file.


    with open('phonebook.in', 'r') as open_file:
        phonebook_lines = open_file.readlines()

    logging.debug(phonebook_lines)
    # reading thru the file
    for idx,phonebook_lines_entry in enumerate(phonebook_lines):
        logging.debug(phonebook_lines_entry)
        if check_email_address(phonebook_lines_entry):
            # this is an e-mail address, adding it and the associated name to the phonebook dictionary.
            logging.debug(phonebook_lines[idx-1])
            phonebook_dictionary[phonebook_lines_entry.strip()] = phonebook_lines[idx-1].strip()
        else:
            logging.debug("not an email!")
    logging.debug(phonebook_dictionary)

    user_choice = application_menu()




    # Checking the validity of our data. Our entries should all include a name and an e-mail address.

    # the regex for an e-mail address (although this is simplified.)







if __name__ == '__main__':
    default_function()





