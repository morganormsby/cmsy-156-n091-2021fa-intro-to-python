import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.DEBUG, format=loggingformat)

def problem_1():
    def times_ten(input_value):
        print("problem 1")
        print("input value is " + str(input_value))
        output_value = input_value * 10
        return(output_value)
    print(times_ten(100))

def problem_2():
    problem2_show_value = show_value(12)

def problem_3():
    a = 3
    b = 2
    c = 1

def problem_4():
    def main():
        print("problem 4")
        x = 1
        y = 3.4
        print(x, y)
        change_us(x, y)
        print(x, y)

    def change_us(a, b):
        a = 0
        b = 0
        print(a, b)

    main()

    # 1 3.4
    # 0 0
    # 1 3.4

def problem_5():
    print("Problem 5")
    def my_function(a, b, c):
        d = (a + c) / b
        print(d)
    print(my_function(2,4,6))

    # this function does not return the expected value - no return statement.
    # 2.0

def problem_6():
    print("problem 6")
    import random
    rand=random.randint(0,100)
    print(rand)

def problem_7():
    print("problem 7")
    def problem_7_function(number):
        return(number*.5)
    print(problem_7_function(100))

def problem_8():
    def cube(num):
        return num * num * num
    result = cube(4)
def problem_9():
    def times_ten(input_value):
        print("problem 9")
        print("input value is " + str(input_value))
        output_value = input_value * 10
        return(output_value)
    print(times_ten(100))

def problem_10():
    print("problem 10")
    def get_first_name():
        return(input("Please enter your first name:\n"))
    print(get_first_name())

if __name__ == '__main__':
    problem_1()
    problem_4()
    problem_5()
    problem_7()
    problem_9()
    problem_10()