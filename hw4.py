import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.DEBUG, format=loggingformat)


# Algorithm Workbench
# 1) Write a while loop that lets the user enter a number. The number should be multiplied by 10, and the result
# assigned to a variable named product. The loop should iterate as long as product is less than 100.

def problem_1():
    logging.debug("Starting execution of function")

    product = 0
    while product < 100:
        input_value = int(input("Please enter a number\n"))
        product = input_value * 10
    print("Product higher then 100.")


# 2) Write a while loop that asks the user to enter two numbers. The numbers should be added and the sum displayed.
# The loop should ask the user if he or she wishes to perform the operation again. If so, the loop should repeat,
# otherwise it should terminate.

def problem_2():
    logging.debug("Starting execution of function")

    repeat_flag = True

    while repeat_flag:
        problem_2_input_number_one = int(input("Please enter the first value\n"))
        problem_2_input_number_two = int(input("Please enter the second value\n"))

        print("The sum of your inputs is: " + str(problem_2_input_number_one+problem_2_input_number_two))
        print("Would you like to run the program again?")
        repeat_application_input_flag = str(input("(y/n)"))

        if repeat_application_input_flag == "n":
            repeat_flag = False


# 3) Write a for loop that displays the following set of numbers:
#
#  0, 10, 20, 30, 40, 50 . . . 1000
def problem_3():
    logging.debug("Starting execution of function")
    for display_value in range(0,1001,10):
        print(display_value)
# 4) Write a loop that asks the user to enter a number. The loop should iterate 10 times and keep a running total
# of the numbers entered.

def problem_4():
    logging.debug("Starting execution of function")
    running_total = 0
    for problem_4_loop_value in range(0,11):
        problem_4_input_value = float(input("Please enter your number"))
        running_total = problem_4_input_value + running_total
        print("The running total is: " + str(running_total))


# 5) Write a loop that calculates the total of the following series of numbers:
#
#
def problem_5():
    problem_5_current_divisor = 30
    for problem_5_current_loop in range(1,31):
        logging.debug(problem_5_current_loop)
        logging.debug(problem_5_current_divisor)

        current_loop_answer = problem_5_current_loop / problem_5_current_divisor
        print(str(problem_5_current_loop) + "/" + str(problem_5_current_divisor) + "=: ")
        print(float(problem_5_current_loop/problem_5_current_divisor))
        problem_5_current_divisor -= 1

# 6) Rewrite the following statements using augmented assignment operators.

def problem_6():
    logging.debug("Starting execution of function")
    # x = x + 1

    x += 1

    # x = x * 2

    x *= 2

    # x = x / 10
    x /= 0

    # x = x − 100

    x -= 100



# 7) Write a set of nested loops that display 10 rows of # characters. There should be 15 # characters in each row.
def problem_7():
    logging.debug("Starting execution of function")
    for problem_7_outer_loop in range(0,11):
        for problem_7_inner_loop in range(0,15):
            print("#", end="")
        print("\n", end="")

# 8) Write code that prompts the user to enter a positive nonzero number and validates the input.
def problem_8():
    logging.debug("Starting execution of function")
    validated_input = False
    while not validated_input:
        try:
            problem_8_input_value = int(input('Enter number: '))
            if problem_8_input_value > 0:
                validated_input = True
        except ValueError:
            print("We got a bad value.")

# 9) Write code that prompts the user to enter a number in the range of 1 through 100 and validates the input.

def problem_9():
    logging.debug("Starting execution of function")
    problem_9_input_value = int(input("Please enter a number:\n"))
    if problem_9_input_value in range(1,101):
        print("True!")
