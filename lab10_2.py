import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.DEBUG, format=loggingformat)

# Write a class named Employee that holds the following data about an employee in attributes:
# name
# ID number
# department
# job title.
# Don't include a constructor or any other methods.
# Once you have written the class, write a program that creates three Employee objects to hold the following data:
# Name ID Number Department Job Title
# Susan Meyers 47899 Accounting Vice President
# Mark Jones 39119 IT Programmer
# Joy Rogers 81774 Manufacturing Engineering
# The program should store this data in three Employee objects and then print the data for each employee.

def default_function():
    logging.debug("Starting execution of function")

class employee():

    def __init__(self, __employee_name, __employee_id, __employee_department, __employee_title):
        self.__employee_name = __employee_name
        self.__employee_id = __employee_id
        self.__employee_department = __employee_department
        self.__employee_title = __employee_title

    logging.debug('Yay!')


test_employee = employee("name",'id', 'dept','title')

logging.debug(test_employee.employee_name)

if __name__ == '__main__':
    default_function()

    import json


    class Laptop:
        name = 'My Laptop'
        processor = 'Intel Core'


    # create object
    laptop1 = Laptop()
    laptop1.name = 'Dell Alienware'
    laptop1.processor = 'Intel Core i7'

    # convert to JSON string
    jsonStr = json.dumps(laptop1.__dict__)

    # print json string
    print(jsonStr)