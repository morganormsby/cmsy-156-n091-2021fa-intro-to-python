import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.CRITICAL, format=loggingformat)

def default_function():
    logging.debug("Starting execution of function")

    # Lets define some functions to get us instructor, room, and time:

    def get_course_room(course_number):

        course_room = {
            'CS101': 3004,
            'CS102': 4501,
            'CS103': 6755,
            'NT110': 1244,
            'CM241': 1411
        }

        for key in course_room:
            if key.lower() == course_number:
                logging.debug(course_room[key])
                return(course_room[key])


    def get_course_instructor(course_number):

        course_instructor = {
            'CS101': 'Haynes',
            'CS102': 'Alvarado',
            'CS103': 'Rich',
            'NT110': 'Burke',
            'CM241': 'Lee'
        }

        for key in course_instructor:
            if key.lower() == course_number:
                logging.debug(course_instructor[key])
                return(course_instructor[key])

    def get_course_time(course_number):

        course_times = {
            'CS101': '8:00am',
            'CS102': '9:00am',
            'CS103': '10:00am',
            'NT110': '11:00am',
            'CM241': '1:00pm'
        }

        for key in course_times:
            if key.lower() == course_number:
                logging.debug(course_times[key])
                return(course_times[key])


    # testing code.

    test_course = 'CS103'
    logging.debug(get_course_room(test_course))
    logging.debug(get_course_instructor(test_course))
    logging.debug(get_course_time(test_course))


    # lets get the users input:
    execute_course = input('Please enter a course number.')

    # lets make sure the user entered a good course number:
    if not get_course_time(execute_course):
        logging.debug('Course not found.')
        print('Course Not Found, Exiting.')

    # lets print the course details using our functions we defined earlier.
    print("Course Details:\n"
          "Course Room Number:  " + str(get_course_room(execute_course)) + "\n"
          "Course Instructor:   " + get_course_instructor(execute_course) + "\n"
          "Course Meeting Time: " + get_course_time(execute_course)
          )


# Execute:
if __name__ == '__main__':
    default_function()