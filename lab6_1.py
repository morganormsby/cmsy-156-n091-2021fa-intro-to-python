import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.CRITICAL, format=loggingformat)

# 1. Tennis Scores
# The Columbia Tennis Association has a tournament every weekend. The
# association president asked you to write two programs:
# A. A program that will read each player's First name, last name, games
# won, matches won and ranking as keyboard input, then save these as
# records in a file named Tennis. txt. (Each record First name, last name,
# games won, matches won and ranking. Please make sure you enter
# at least 15 names of players who participated in the tournament)
# B. A program that reads the records from the tennis.txt file and displays
# them. Please include .py and screenshots of the output in your
# submission.


def write_function():
    import json
    logging.debug("Starting execution of function")

    # lets define our variables for the app:
    output_to_file = []
    current_player_first_name = None
    current_player_last_name = None
    current_player_games_won = None
    current_player_matches_won = None
    current_player_ranking = None

    repeat_flag = True

    while repeat_flag:
        # lets prompt for the individual player data:
        current_player_data = {}
        current_player_data['current_player_first_name'] = ("Please enter player first name:")
        current_player_data['current_player_last_name'] = ("Please enter player last name:")
        current_player_data['current_player_games_won'] = ("Please enter player games won:")
        current_player_data['current_player_matches_won'] = ("Please enter player matches won:")
        current_player_data['current_player_ranking'] = ("Please enter player ranking:")

        # lets put these inputted values into a list and see if there's another entry:
        output_to_file.append(current_player_data)
        if input("another record? (y/n)") == 'n':
            repeat_flag = False

    logging.debug(output_to_file)

    # sending that list to the file:
    with open('tennis.txt', 'w') as outfile:
        json.dump(output_to_file, outfile)

def read_function():
    import json

    # Lets open the file back up:
    with open('tennis.txt', 'r') as open_file:
        data_from_file = json.load(open_file)

    logging.debug(data_from_file)

    # lets loop over the player entries and write them out:
    for player_entry in data_from_file:
        logging.debug(player_entry)
        logging.debug(player_entry['current_player_first_name'])

        # writing them to the console:
        print("First Name: " + player_entry['current_player_first_name'] + ", Last Name: " +
              player_entry['current_player_last_name'] + ", Games Won: " +
              player_entry['current_player_games_won'] + ", Matches Won: " +
              player_entry['current_player_matches_won'] + ", Ranking: " + player_entry['current_player_ranking'])


if __name__ == '__main__':
    write_function()

if __name__ == '__main__':
    read_function()