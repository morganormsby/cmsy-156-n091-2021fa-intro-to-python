import logging
import datetime
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.CRITICAL, format=loggingformat)

# Average Rainfall: Write a program that uses nested loops to collect data and calculate the average rainfall over a
# period of years. The program should first ask for the number of years. The outer loop will iterate once for each
# year. The inner loop will iterate twelve times, once for each month. Each iteration of the inner loop will ask the
# user for the inches of rainfall for that month. After all iterations, the program should display the number of
# months, the total inches of rainfall, and the average rainfall per month for the entire period.

def default_function(number_of_years = None):
    logging.debug("Starting execution of function")

    if number_of_years == None:
        logging.debug("looks like we didn't get any arguments passed to the function. Prompting user:")
        # lets prompt the user for number of years:
        number_of_years = int(input("Enter the number of years you'd like to calculate rainfall for.\n"))

    # lets get number of months, we'll need this later for average.
    number_of_months = number_of_years * 12

    # lets initalize the variable for total_rainfall.
    total_rainfall = 0

    # lets use a loop to iterate over the years.
    for current_year in range(1,number_of_years + 1):
        logging.debug(current_year)
        # another loop for the months in those years.
        for current_month in range(1,13):
            logging.debug(current_month)
            # lets get a human readable name for the month we're working on:
            month_name = datetime.date(1900, current_month, 1).strftime('%B')
            logging.debug(month_name)
            # lets prompt the user for the rainfall for this month.
            prompt_text = ("Please enter the total rain fall (in inches) for " + month_name + " of year # " +
                           str(current_year) + ": ")
            this_month_rainfall = int(input(prompt_text))
            # lets add this rainfall to the total rainfall.
            total_rainfall = total_rainfall + this_month_rainfall
            logging.debug(total_rainfall)

    logging.debug(number_of_months)

    # we're gonna do the math to get the average rainfall.
    monthly_average = total_rainfall / number_of_months

    # and finally, lets print out the result:
    print("the Total rainfall across " + str(number_of_years) + "year (" + str(number_of_months) + " months) is " +
          str(total_rainfall) + " inches")
    print("for an average of " + str(monthly_average) + " inches monthly")


if __name__ == '__main__':
    default_function()
