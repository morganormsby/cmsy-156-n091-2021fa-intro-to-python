import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.CRITICAL, format=loggingformat)

# Lab 2.2

# Write a program that asks the user for the number of males and the number of females registered in a class using
# two separate inputs ("Enter number of males:", "Enter number of females:"). The program should display the
# percentage of males and females (round to the nearest whole number) in the following format:
# Percent males: 35%
# Percent females: 65%

# Use string formatting.


def default_function(number_of_girls=None,number_of_boys=None):
    logging.debug("Starting.")

    # lets check to see if we were passed arguments to the function:

    if not number_of_girls or not number_of_boys:
        # We didn't have anything in these variables, assuming we got no arguments, lets get input from the keyboard.
        logging.debug("Looks like we were not passed any student counts in the function, guess we're running directly?")

        number_of_girls = input("Enter number of females:\n")
        number_of_boys = input("Enter number of males:\n")
    # lets try to add the girls and boys together - if we fail, it's probably casue we got something that's not a
    # number.
    try:
        total_students = int(number_of_girls) + int(number_of_boys)
    except (TypeError, ValueError):
        print("Sure you entered numbers for number of females/males?")

    # lets get percentage of girls and round the result.:
    percentage_of_females = round(int(number_of_girls) / total_students * 100)
    # lets get percentage of boys and round the result.:
    percentage_of_males = round(int(number_of_boys) / total_students * 100)

    # lets print those values to the screen.

    print("Percent Males: " + str(percentage_of_males) + "%")
    print("Percent Females: " + str(percentage_of_females) + "%")



