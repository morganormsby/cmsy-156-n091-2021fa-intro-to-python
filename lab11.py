import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.DEBUG, format=loggingformat)

def default_function():
    logging.debug("Starting execution of function")
    import json


    class Employee:
        def __init__(self, __employee_name, __employee_number):
            self.__employee_name = __employee_name
            self.__employee_number = __employee_number

        def get_employee_number(self):
            return self.__employee_number

        def get_employee_name(self):
            return self.__employee_name

    class ProductionWorker(Employee):
        def __init__(self, __shift_number, __hourly_payrate, __employee_name, __employee_number):
            super().__init__(__employee_name, __employee_number)
            self.__shift_number = __shift_number
            self.__hourly_payrate = __hourly_payrate

        def get_production_worker_payrate(self):
            return self.__hourly_payrate

        def get_production_worker_shift_number(self):
            return self.__shift_number

        def give_hourly_raise(self, payraise):
            self.__salary_payrate = self.__hourly_payrate + payraise
            return self.__hourly_payrate

        def get_productionworker_yearly_compensation(self):
            hoursworked = input("enter number of hours worked this year")
            return self.__hourly_payrate * hoursworked


    class Supervisor(Employee):
        def __init__(self, __yearly_bonus, __salary_payrate, __employee_name, __employee_number):
            super().__init__(__employee_name, __employee_number)
            self.__yearly_bonus = __yearly_bonus
            self.__salary_payrate = __salary_payrate

        def get_supervisor_salary_payrate(self):
            return self.__salary_payrate

        def get_supervisor_yearly_bonus(self):
            return self.__yearly_bonus

        def get_supervisor_yearly_compensation(self):
            return self.__yearly_bonus + self.__salary_payrate

        def give_salary_raise(self, payraise):
            self.__salary_payrate = self.__salary_payrate + payraise
            return self.__salary_payrate


    # Alrighty, our demo app is an employee database tool. It will allow the user to enter employees

    employee_data = []
    menu_entry = "0"
    while menu_entry is '0':
        print(""
              "1 - Write to File\n"
              "2 - Read from file\n"
              "3 - Add employee\n"
              "4 - search for employee\n"
              "5 - give employee raise\n"
              "6 - get employee yearly compensation\n"
              "7- fire employee\n")
        menu_entry = input('make an entry:\n')

        if menu_entry is 1:
            # todo: function to write to file:
            for entry in employee_data:

                jsonStr = json.dumps(entry.__dict__)

                with open('employee_database.txt',a) as employee_database:
                    employee_database.write(jsonStr)

        elif menu_entry is 2:

            # todo: function to read from the file.
            # load json
            # class Person():
            #     def __init__(self, name, age):
            #         self.name = name
            #         self.age = age

            employee_database = open('employee_database.txt', 'r')
            employee_database_lines = employee_database.readlines()

            for entry in employee_database_lines:
                employee_dict = json.loads(entry)
                employee_object = Employee(**employee_dict)
            #
            # person_string = '{"name": "Bob", "age": 25}'
            #
            # person_dict = json.loads(person_string)
            # person_object = Person(**person_dict)

        # todo: menu function

        elif menu_entry is 3:
            # add employee
            logging.debug('adding employee')
            add_menu_entry = "0"
            while add_menu_entry is '0':
                print("enter employee type")

                if add_menu_entry is "1":
                    logging.debug('production')

                elif add_menu_entry is "2":
                    logging.debug('supervisor')
            new_employee_name = input("Enter Name:\n")


        elif menu_entry is 4:
            logging.debug("search!")

        elif menu_entry is 5:
            logging.debug("raise!")

        elif menu_entry is 6:
            logging.debug("yearly pay")

        elif menu_entry is 7:
            logging.debug("fired!")

        else:
            print("Invalid Entry! Try again,\n")

    mike = ProductionWorker(0,15,'mike',10)
    bob = Supervisor(10,60000,'bob',20)

    print(mike.get_employee_name())

    print(bob.get_employee_name())

    print(bob.give_raise(10000))



if __name__ == '__main__':
    default_function()