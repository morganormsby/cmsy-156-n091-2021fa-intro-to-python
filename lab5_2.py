import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.CRITICAL, format=loggingformat)

# Paint Job Estimator:
# A painting company has determined that for every 112 square feet of wall space, one gallon of paint and eight
# hours of labor will be required. The company charges $35.00 per hour for labor. Write a program that asks the user
# to enter the square feet of wall space to be painted and the price of the paint per gallon. The program should
# display the following data:
# • The number of gallons of paint required
# • The hours of labor required
# • The cost of the paint
# • The labor charges
# • The total cost of the paint job


def default_function(sqft_to_paint = None,paint_cost_per_gallon = None):
    logging.debug("Starting execution of function")

    if not sqft_to_paint or not paint_cost_per_gallon:
        #looks like no values were passed to the function, prompting:
        sqft_to_paint = float(input("Please enter the number of SQFT to paint:\n"))
        paint_cost_per_gallon = float(input("Please enter the cost per gallon of paint\n"))

    # Imports
    import math

    # Lets calculate the number of units of work to do,and round up to the cloest whole value - Since the problem says
    # specifically "for every 110 sqft oof wall space it needs 8 hours of work". We'll interprete that to mean that we
    # cannot divide these values possibly becasue of setup and take down or such that isn't easily divisible.
    work_units = math.ceil(sqft_to_paint / 110)
    logging.debug(work_units)

    # each unit of work needs one gallon of paint, that's easy math:
    paint_required = work_units

    #Every work unit needs 8 hours of labor:
    labor_required = work_units * 8

    # lets calcualte the labor cost of the job:
    total_labor_cost = labor_required * 35

    # lets calcualte the cost of paint:
    total_paint_cost = paint_cost_per_gallon * work_units

    # lets get the total job cost:
    total_job_cost = total_labor_cost + total_paint_cost

    #debugging these values:
    logging.debug(paint_required)
    logging.debug(labor_required)
    logging.debug(total_paint_cost)
    logging.debug(total_labor_cost)
    logging.debug(total_job_cost)


    # lets print a visually friendly breakdown of the project. This assumes the output view is larger then 80 characters wide.
    print("Job Breakdown:")
    print("Number of gallons of paint required:               " + str(paint_required) + " gals")
    print("The hours of labor required                        " + str(labor_required) + " hrs")
    print("The cost of the paint                              $" + str(total_paint_cost))
    print("Labor:                                             $" + str(total_labor_cost))
    print('--------------------------------------------------------------')
    print("Job Total:                                         $" + str(total_job_cost))
    print()
    print()
    print()
    print()


if __name__ == '__main__':
    default_function()