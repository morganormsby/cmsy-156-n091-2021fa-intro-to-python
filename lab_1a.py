

def default_function():
    a = 4
    b = 5
    print("13:")
    print(a + b)

    n = 7
    n += 1
    print("14:")
    print(1, n, n + 1)

    num = 5
    num *= 2
    print("15:")
    print(num)

    tax = 200
    tax - 25 + tax
    print("16:")
    print(tax)

    totalMinutes = 135
    hours = totalMinutes // 60
    minutes = totalMinutes % 60
    print("17:")
    print(hours, minutes)

    totalOunces = 90
    pounds = totalOunces // 16
    ounces = totalOunces %16
    print("18:")
    print(pounds, ounces)

    radius = 15
    area = (radius ** 2) * 3.14159265359
    print("19:")
    print("Radius:" + str(radius))
    print(area)

    length = 25
    width = 20
    print(length*width)

if __name__ == '__main__':
    default_function()