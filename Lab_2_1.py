import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.CRITICAL, format=loggingformat)

# Lab 2.1

# A cookie recipe calls for the following ingredients:
# • 1.5 cups of sugar
# • 1 cup of butter
# • 2.75 cups of flour
# The recipe produces 48 cookies with this amount of ingredients. Write a program that
# asks the user how many cookies they want to make and then displays the number of cups
# of each ingredient needed for the specified number of cookies in the following format:
# You need 5 cups of sugar, 3 cups of butter, and 7 cups of flour.


def default_function(number_of_cookies_to_make=None):
    logging.debug("Starting.")

    recipe_sugar = 1.5
    recipe_butter = 1
    recipe_flour = 2.75
    recipe_cookies = 48
    conversion_factor = 1

    # We're gonna see if we were passed a number of cookies on the function, or we need to prompt the user:
    if number_of_cookies_to_make:
        # looks like we were passed a value from the function. We're still testing, lets set the debugger to debug.
        in_testing = True
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        in_testing = False
        # Looks like we did not get a number passed via the function, lets prompt the user:
        logging.debug("We were not passed a function variable - Prompting user for number of cookies.")
        print("Lets make some cookies!")
        number_of_cookies_to_make = input("How many Cookies would you like to make?\n")
    if number_of_cookies_to_make == 0:
        logging.debug("We're not making any cookies??!!")
    # Lets see if the value we got looks like a number and isn't zero. At the same time, we're gonna
    # get our conversion factor for the recipe.
    logging.debug(number_of_cookies_to_make)
    try:
        # making sure that number_of_cookies_to_make is indeed a number
        number_of_cookies_to_make = int(number_of_cookies_to_make)
        # lets do the math to get the conversion factor for the recipe.
        conversion_factor = number_of_cookies_to_make / recipe_cookies
        logging.debug(conversion_factor)
    except TypeError:
        logging.debug("Looks like the user didn't enter a number or our number formatting broke.")
        print("Looks like we didn't get a number or we had some other error.")
        return()
    except:
        logging.debug("Couldn't do our math on conversion factor for some reason.")
        return()
    # Alrighty - pretty sure we have a good number at this point - lets do the math.

    # We're gonna multiply the ingredients with the conversion factor to get the required amounts.

    output_sugar = recipe_sugar * conversion_factor
    output_butter = recipe_butter * conversion_factor
    output_flour = recipe_flour * conversion_factor

    logging.debug(output_sugar)
    logging.debug(output_butter)
    logging.debug(output_flour)

    # and now we're gonna output the necessary ingredients for our tasty cookies.

    #if we're testing, lets output to the function via a dict.
    if in_testing is True:
        output_dict = {'sugar': output_sugar, 'butter': output_butter, 'flour': output_flour}
        return output_dict

    else:
        logging.debug("We're not in testing!")

    # lets print the result out to the prompt.
    print("'You need %.2f cups of sugar, %.2f cups of butter, and %.2f cups of flour.'" %
          (output_sugar, output_butter,output_flour))


if __name__ == '__main__':
    default_function()