import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.DEBUG, format=loggingformat)

def default_function():
    logging.debug("Starting execution of function")

if __name__ == '__main__':
    default_function()