import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.DEBUG, format=loggingformat)

def problem_1():
    logging.debug("Starting execution of function")
    problem_1_file = open("myname.txt", "a")
    problem_1_file.write("Morgan Ormsby")
    problem_1_file.close()

def problem_2():
    problem_2_file = open("myname.txt", "r")
    print(problem_2_file.read())
    problem_2_file.close()

def problem_3():
    problem_3_file = open("number_list.txt", "a")
    for x in range(1,101):
        problem_3_file.write(str(x) + "\n")
    problem_3_file.close()

def problem_4():
    problem_4_file = open("number_list.txt", "r")
    print(problem_4_file.read())
    problem_4_total = 0
    for number_from_file in problem_4_file.readlines():
        logging.debug(number_from_file)
        number_from_file += problem_4_total
    print(problem_4_total)


    problem_4_file.close()

if __name__ == '__main__':
    problem_1()
    problem_2()
    problem_3()
    problem_4()