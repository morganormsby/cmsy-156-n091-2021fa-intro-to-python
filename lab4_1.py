import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.CRITICAL, format=loggingformat)


# 1.
# At one college, the tuition for a full-time student is $8,000 per semester. It has been announced that the tuition
# will increase by 3 percent each year for the next 5 years. Write a program with a loop that displays the projected
# semester tuition amount for the next 5 years.
# The program should print out the result in the form
# In 1 year, the tuition will be $8002.3.
# In 2 years, the tuition will be $8103.2.
# In 3 years, ...In 4 years, ...In 5 years, ...
# (If, for example, the tuition would cost 8002.3 dollars in one year, etc.)


def default_function():
    logging.debug("Starting execution of function")

    # lets set the starting tuition, and get the current_tuition set for the loop.
    starting_tuition = 8000
    current_tuition = starting_tuition
    # we need to loop 5 times for the year, lets start the loop on 1 tho, so we can use it for the year number.
    for current_year in range(1,6):
        logging.debug(current_year)
        # lets do the math on the current_tuition and round it out to two decimals.
        current_tuition = current_tuition * 1.03
        current_tuition = round(current_tuition,2)
        logging.debug(current_tuition)
        # I'm lazy :D so lets just seperate the first year from the others, so we can get the plural year right.
        if current_year == 1:
            print("In " + str(current_year) + " year, the tuition will be $" + str(current_tuition) + ".")
        else:
            print("In " + str(current_year) + " years, the tuition will be $" + str(current_tuition) + ".")

if __name__ == '__main__':
    default_function()
