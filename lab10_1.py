import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.CRITICAL, format=loggingformat)


def default_function():
    logging.debug("Starting execution of function")


class Car:
    def __init__(self, __year_model, __make, __speed):
        self.__year_model = __year_model
        self.__make = __make
        self.__speed = 0


    def accel(self):
        self.__speed += 5
        logging.debug("speed!")

    def brake(self):
        self.__speed -= 5
        logging.debug(self.__speed)

    def get_speed(self):
        return self.__speed

mytruck = Car(2006,"Ford",0)

for i in range(4):
    mytruck.accel()
    print(mytruck.get_speed())

for i in range(4):
    mytruck.brake()
    print(mytruck.get_speed())


if __name__ == '__main__':
    default_function()