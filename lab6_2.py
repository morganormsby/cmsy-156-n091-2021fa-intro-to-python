import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.CRITICAL, format=loggingformat)

# 2. Average Steps Taken
# A Fitbit Charge 2 Tracker is a wearable device that tracks your physical activity, calories burned, heart rate,
# sleeping patterns, and so on. One common physical activity that most of these devices track is the number of steps
# you take each day. Please use the steps.txt file that is provided in this assignment. The steps. txt file contains
# the number of steps a person has taken each day for a year. There are 366 lines in the file, and each line contains
# the number of steps taken during a day. (The first line is the number of steps taken on January 1st, the second line
# is the number of steps taken on January 2nd, and so forth.) Write a program that reads the file, then displays the
# average number of steps taken for each month. (The data is from a 2016 which has a leap year, so February has 29
# days.) Please include .py and screenshots of the output in your submission.

def default_function():
    import datetime
    from calendar import monthrange
    logging.debug("Starting execution of function")

    with open('steps.txt', 'r') as open_file:
        step_data = open_file.readlines()

        logging.debug(step_data)


        results_dictionary = {}
        results_dictionary['01'] = 0
        results_dictionary['02'] = 0
        results_dictionary['03'] = 0
        results_dictionary['04'] = 0
        results_dictionary['05'] = 0
        results_dictionary['06'] = 0
        results_dictionary['07'] = 0
        results_dictionary['08'] = 0
        results_dictionary['09'] = 0
        results_dictionary['10'] = 0
        results_dictionary['11'] = 0
        results_dictionary['12'] = 0
        for idx,step_entry in enumerate(step_data, start=1):
            logging.debug(idx)
            logging.debug(step_entry)
            time_string = "2016 " + str(idx)
            logging.debug(time_string)
            logging.debug(datetime.datetime.strptime(time_string, '%Y %j').strftime("%m-%d-%Y"))
            current_line_month = datetime.datetime.strptime(time_string, '%Y %j').strftime("%m")
            logging.debug(current_line_month)

            results_dictionary[current_line_month] = int(results_dictionary[current_line_month]) + int(step_entry)

        logging.debug(results_dictionary)

        for results_month in results_dictionary:
            logging.debug(results_month)
            number_of_days_in_month = monthrange(2016, int(results_month))[1]
            logging.debug(number_of_days_in_month)
            logging.debug(results_dictionary[results_month])
            average_steps = int(results_dictionary[results_month]) / int(number_of_days_in_month)
            average_steps = int(average_steps)
            month_name = datetime.date(2016, int(results_month), 1).strftime('%B')
            print("the average for the month of " + month_name + " is " + str(average_steps))

if __name__ == '__main__':
    default_function()