import logging
loggingformat = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(level=logging.CRITICAL, format=loggingformat)

# Problem 2: ColorMixer
# The colors red, blue, and yellow are known as the primary colors because they cannot be made by mixing other colors.
# When you mix two primary colors, you get a secondary color:
# When you mix red and blue, you get purple.
# When you mix red and yellow, you get orange.
# When you mix blue and yellow, you get green.
# Design a program that prompts the user to enter the names of two primary colors,one at a time. If the user enters
# anything other than "red," "blue," or "yellow," the program should print "You didn't input two primary colors."
# Otherwise, it should print something in the format:
# "When you mix red and blue, you get purple."(Assuming the user entered "red" and "blue”.)


def default_function(color_1=None,color_2=None):
    logging.debug("Starting execution of function")

    # lets define our primary colors.
    primary_colors = ['red','blue','yellow']

    # lets check to see if we were passed args directly into the function
    if color_1 == None or color_2==None:
        # looks like the answer is no, lets prompt that user for their colors:
        logging.debug("we didn't get passed args in the function, prompting user:")

        color_1 = str(input("Please enter you first color\n"))
        color_2 = str(input("Please enter your second color.\n"))

    try:
        # lets see if our inputs will handle being converted into strings and made lower case:
        color_1 = str(color_1).lower()
        color_2 = str(color_2).lower()

        # next, lets make sure the text we got was actually a primary color.
        if not color_1 in primary_colors or not color_2 in primary_colors:
            logging.debug("something wasn't a primary color - printing that result.")
            print("You didn't input two primary colors.")
            return
    except(TypeError, ValueError):
        # hmm, looks like we didn't get a good value in our inputs somewhere along the way.
        print("You didn't input two primary colors...Perhaps we get something other then text in the inputs?")
    # LEts check to see is we got identical colors.
    if color_1 == color_2:
        print("Mixing " + color_1 + " with " + color_2 + " is just gonna give you boring old " + color_1)
    # We'll start with checking the variations of blue.
    if color_1 == 'blue' or color_2 == 'blue':
        logging.debug("a color was blue!")
        # blue and red:
        if color_1 == 'red' or color_2 == 'red':
            print("When you mix " + color_1 + " and " + color_2 + ', you get purple!')
            return
        # blue and yellow:
        elif color_1 == 'yellow' or color_2 == 'yellow':
            print("When you mix " + color_1 + " and " + color_2 + ', you get green!')
            return
    # looks like nothing was blue - lets see if it was yellow and red:
    elif color_1 == 'yellow' or color_2 == 'yellow':
        logging.debug("A color was yellow!")
        print("When you mix " + color_1 + " and " + color_2 + ', you get orange!')


if __name__ == '__main__':
    default_function()